## Debugging Commands
You can use the following commands for a running container:
```sh
$ kubectl logs <pod-name>
$ kubectl exec -it <pod-name> -- bash # This will provide you with an interactive shell inside the running container so that you can perform more debugging.
$ kubectl attach -it <pod-name> # If you don’t have bash or some other terminal available within your container, you can always attach to the running process
$ kubectl cp <pod-name>:</path/to/remote/file> </path/to/local/file> # You can also copy files to and from a container using the cp command
$  kubectl describe <resource-name> <obj-name> # If you are interested in more detailed information about a particular object, use the describe command
```
Finally, if you are interested in how your cluster is using resources, you can use the top command to see the list ofresources in use by either nodes or Pods. This command:
```sh 
$ kubectl top nodes
```
Will display the total CPU and memory in use by the nodes in terms of both absolute units (e.g., cores) and percentageof available resources (e.g., total number of cores). Similarly, this command:
```sh
kubectl top pods
```
Will show all Pods and their resource usage. By default it only displays Pods in the current namespace, but you canadd the --all-namespaces flag to see resource usage by all Pods in the cluster.
