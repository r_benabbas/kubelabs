# Tested On Ubuntu 20.04.1 LTS

# Scripts
Install Docker, Installing Kubeadm, Kubelet, and Kubectl on all nodes
```sh
$ docker-in.sh
$ components.sh
```
Bootstrapping the Cluster, run the following command only in the master node
```sh
$ sudo kubeadm init --pod-network-cidr= 10.244.0.0/16
```
To enable tab completion in bash, you’ll first need to install a package called bash-
completion and then run the following command (you’ll probably also want to add it to
~/.bashrc or equivalent):
```sh
$ echo "source <(kubectl completion bash)" >> .bashrc
```
When it is done, set up the local kubeconfig
```sh
$ mkdir -p $HOME/.kube
$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/configsudo chown $(id -u):$(id -g) $HOME/.kube/config
```
The kubeadm init command should output a kubeadm join command containing a
token and hash. Copy that command and run it with sudo on both worker nodes. It
should look something like this: 
```sh
$ sudo kubeadm join $some_ip:6443 --token $some_token --discovery-token-ca-
cert-hash $some_hash
```
If you lose the the join token, you can generated by 
```sh
$ kubeadm token create --print-join-command 2> /dev/null
```
