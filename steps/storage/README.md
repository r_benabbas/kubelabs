# Storage
Volumes
```sh
$ kubectl create -f volume.yaml
$ kubectl exec -it volumepod -c centos1 -- touch /centos1/myfile # Create the test file
$ kubectl exec -it volumepod -c centos2 -- ls /centos2/myfile # List the test file
```
Persistente storage
```sh
$ kubectl create -f pv.yaml  # Create the Persistente volume
$ kubectl get pv
$ kubectl create -f pvc.yaml # Create pv claim
$ kubectl get pvc 
$ kubectl create -f pv-pod.yaml # Create pod with pv
$ kubectl exec -it pv-pod -- /bin/bash 
$ root@pv-pod:~# echo "it's me" > /usr/share/nginx/html/index.html  # create index file to test
$ curl $pod_ip
```
ConfigMaps
```sh
$ kubectl create configmap nginx-cm --from-file nginx-custom-config.conf
$ kubectl get configmaps -o yaml # to see the yaml file
$ kubectl create -f nginx-cm.yaml # Create the pod, that copy the key to default.conf
$ kubectl exec -it nginx-cm -- /bin/bash
$ root@nginx-cm:/# cat /etc/nginx/conf.d/default.conf # Check the copied conf
$ curl $pod_ip:8888
```
Using Secrets
```sh
$ kubectl create secret generic lab --from-literal=password=me --from-literal=user=you
$ kubectl get secrets lab -o yaml
$ echo bWU= | base64 -d  # To show your password 
$ kubectl create -f pod-secret.yaml # Create pod to use the secret
$ kubectl exec -it secretbox -- /bin/sh
/ # cat lab/password;cat /lab/user # show the user and password 
```
