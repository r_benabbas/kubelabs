# Services
ClusterIP
```sh
$ kubectl create -f nginx-deployment.yaml
$ kubectl expose deployment nginx-deployment --port=80 # Create the clusterIP
$ kubectl get svc # You can use curl to the IP 
$ kubectl describe svc nginx-deployment # there is on replica (one Endpoints)
```
Node port 
```sh
$ kubectl edit service nginx-deployment # change targetPort to nodePort:$port and Type to NodePort
$ curl $private_ip:$port
```
Loadbalancer
```sh
$ kubectl expose deployment nginx-deployment --type=LoadBalancer --name=nginx-service --port=80 --external-ip=$external_ip
```
Ingress
https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/
