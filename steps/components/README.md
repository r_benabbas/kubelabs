# Pod, ReplicaSet and Deployment
Create your pod
```sh
$ kubectl create -f httpd-pod.yaml
```
Manage your pod
```sh
$ kubectl get pods -o wide # List all pods with details
$ kubectl describe pods httpd-demo # show the info 
$ kubectl get pods httpd-demo --dry-run -o yaml # Display the yaml config 
$ kubectl exec -it httpd-demo -- /bin/bash
$ kubectl logs httpd-demo  # Show the logs 
$ kubectl delete pods httpd-demo # Delete your pod 
```
Create your deployment
```sh
$ kubectl create -f httpd-deployment.yaml
```
Manage your deployment
```sh
$ kubectl get deployments.apps -o wide # List all deployment with details
$ kubectl get replicaset # Display the replicats 
$ kubectl edit deployments httpd-depdemo # Change the number of replicas
$ kubectl scale deployment --replicas=4 httpd-depdemo   
$ kubectl delete deployments httpd-depdemo # Delete your deployment
```
If you want to secure your environnement you can create the namespace and added in your yaml files (namespace:secret under metatdata)
```sh
$ kubectl create namespace secret 
```
Label your deployment 
```sh
$ kubectl label deployments httpd-depdemo env=dev
$ kubectl get all --selector env=dev # Show only the labled object
```
Managing Update, we create a deployment rolling-nginx with RollingUpdate strategy
```sh
$ kubectl create -f rolling.yml
$ kubectl rollout history deployment rolling-nginx # Check the history
$ kubectl edit deployments.apps rolling-nginx # Edit and change the nginx version to 1.15
$ kubectl rollout history deployment rolling-nginx --revision=2
$ kubectl rollout undo deployment rolling-nginx # rollback to the pervious version
```
Liveness Probe
```sh
$ kubectl create -f kuard-pod-health.yaml
```
The preceding Pod manifest uses an httpGet probe to perform an HTTP GET request against the /healthy endpoint on port 8080 of the kuard container. The probe sets an initialDelaySeconds of 5, and thus will not be called until 5 seconds after all the containers in the Pod are created. The probe must respond within the 1-second timeout, and the HTTP status code must be equal to or greater than 200 and less than 400 to be considered successful. Kubernetes will call the probe every 10 seconds. If more than three consecutive probes fail, the container will fail and restart.
Readiness Checks
```sh
$ kubectl get pods --show-labels
```
This sets up the Pods this deployment will create so that they will be checked for readiness via an HTTP GET to /ready on port 8080. This check is done every 2 seconds starting as soon as the Pod comes up. If three successive checks fail, then the Pod will be considered not ready. However, if only one check succeeds, the Pod will again be considered ready.
Replicaset
```sh
$ kubectl create -f kuard-rs.yaml
$ Kubectt get rs
```
Scaling based on CPU usage is the most common use case for Pod autoscaling. Generally it is most useful for request-based systems that consume CPU proportionally to the number of requests they are receiving, while using a relatively static amount of memory.

To scale a ReplicaSet, you can run a command like the following:
```sh
$ kubectl autoscale rs kuard --min=2 --max=5 --cpu-percent=80
```
This command creates an autoscaler that scales between two and five replicas with a CPU threshold of 80%
If you don’t want to delete the Pods that are being managed by the ReplicaSet, you can set the --cascade flag to false to ensure only the ReplicaSet object is deleted and not the Pods:
```sh
$ kubectl delete rs kuard --cascade=false
```
Creating DaemonSets
```sh
$ kubectl apply -f fluentd-ds.yaml
```
A DaemonSet ensures a copy of a Pod is running across a set of nodes in a Kubernetes cluster. DaemonSets are used to deploy system daemons such as log collectors and monitoring agents, which typically must run on every node. DaemonSets share similar functionality with ReplicaSets; both create Pods that are expected to be long-running services and ensure that the desired state and the observed state of the cluster match.
